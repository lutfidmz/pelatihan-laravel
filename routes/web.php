<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();


Route::group(['middleware' => ['auth']], function () {
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('my-profile','HomeController@myProfile')->name('my-profile');
    //setiap route yang di simpan di dalam sini maka memerlukan login terlebih dahulu

    Route::resource('my-loker', 'MyLokerController');

});


