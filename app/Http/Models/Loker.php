<?php


namespace App\Http\Models;


use App\User;

class Loker extends BaseModel
{
    protected $table = 'loker';
    protected $fillable = ['title', 'description', 'available_date', 'user_id'];

    protected $with = ['user'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
