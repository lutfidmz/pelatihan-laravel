<?php


namespace App\Http\Models;


use App\User;

class Lamaran extends BaseModel
{
    protected $table = 'lamaran';

    public function user(){
        return $this->belongsTo(User::class);
    }
}
