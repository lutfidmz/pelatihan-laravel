<?php


namespace App\Http\Models;


use App\User;

class Role extends BaseModel
{
    public function user(){
        return $this->hasMany(User::class,'user_id');
    }
}
