<?php

namespace App\Http\Controllers;

use App\Http\Models\Loker;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class MyLokerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $user_id = Auth::id();
        $lokers = Loker::where('user_id', $user_id)->latest()->paginate(5);
        return view('frontend.loker.index', compact('lokers'))
            ->with('i', (request()->input('page', 1) - 1) * 5);
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('frontend.loker.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
        ]);

        $data = $request->all();
        $data['user_id'] = Auth::id();
//        dd($data);

        Loker::create($data);
        return redirect()->route('my-loker.index')
            ->with('success', 'Job created successfully.');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $loker = Loker::where('id', $id)->first();
//        dd($id);
        return view('frontend.loker.show', compact('loker'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $loker = Loker::where('id', $id)->first();
//        dd($loker);
        return view('frontend.loker.edit', compact('loker'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->validate([
            'title' => 'required',
            'description' => 'required',
            'available_date' => 'required',
        ]);
        $loker = Loker::find($id)->update($request->all());
        return redirect()->route('my-loker.index')
            ->with('success', 'Job updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Loker $loker, $id)
    {
        $loker->find($id)->delete();
        return redirect()->route('my-loker.index')
            ->with('success', 'Product deleted successfully');
    }
}
