<?php

namespace App;

use App\Http\Models\Lamaran;
use App\Http\Models\Loker;
use App\Http\Models\Role;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Ramsey\Uuid\Exception\UnsatisfiedDependencyException;
use Ramsey\Uuid\Uuid;

class User extends Authenticatable
{
    use Notifiable;

    protected $keyType='string';

    public $incrementing = false;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','role_id','address','phone_number','gender','dob'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];
    //fungsi untuk mengatur apakah mau di set eager loading atau lazy loading
    //jika diisi maka akan menjadi eager loading
    protected $with=['role'];

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            try {
                $model->id = Uuid::uuid4()->toString();
            } catch (UnsatisfiedDependencyException $e) {

            }
        });
    }

    public function role(){
        return $this->belongsTo(Role::class);
    }

    public function lamaran(){
        return $this->hasMany(Lamaran::class);
    }

    public function loker(){
        return $this->hasMany(Loker::class);
    }
}
