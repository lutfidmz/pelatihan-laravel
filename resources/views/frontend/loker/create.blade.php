@extends('layouts.app')

@section('content')
    <div class="container">

        <div class="row">
            <div class="col-lg-12 margin-tb">
                <div class="float-left">
                    <h2>Add New Job</h2>
                </div>
                <div class="float-right">
                    <a class="btn btn-primary" href="{{ route('my-loker.index') }}"> Back</a>
                </div>
            </div>
        </div>
        @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Whoops!</strong> There were some problems with your input.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif

        {!! Form::open(['route'=>'my-loker.store', 'method'=>'POST']) !!}
        {{--        <form action="{{ route('my-loker.store') }}" method="POST">--}}
        @csrf
        <div class="row">
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group">
                    <strong>Title:</strong>
                    {!! Form::text('title',null,array('class'=>'form-control','placeholder'=>'Name') ) !!}
                </div>
            </div>
            <div class="col-xs-6 col-sm-6 col-md-6">
                <div class="form-group">
                    <strong>Available Date:</strong>
                    {!! Form::date('available_date',null,['class'=>'form-control','required'=>'true']) !!}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Description:</strong>
                    {!! Form::textarea('description',null,['class'=>'form-control','required'=>'true','autocomplete'=>'off','rows'=>'3','placeholder'=>'Description']) !!}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                {!! Form::submit('Submit',array('class'=>'btn btn-primary')) !!}
            </div>
        </div>
        {!! Form::close() !!}
        {{--        </form>--}}
    </div>
@endsection
