<?php
//echo '<pre>';
////    var_dump($loker->);
//echo '</pre>';
//
//    die();
//?>
@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <div class="float-left">
                    <h2> Show Job Vacancy</h2>
                </div>
                <div class="float-right">
                    <a class="btn btn-primary" href="{{ route('my-loker.index') }}"> Back</a>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Title:</strong>
                    {{ $loker->title }}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Available Date:</strong>
                    {{ $loker->available_date }}
                </div>
            </div>
            <div class="col-xs-12 col-sm-12 col-md-12">
                <div class="form-group">
                    <strong>Description:</strong>
                    {{ $loker->description }}
                </div>
            </div>
        </div>
    </div>

@endsection
