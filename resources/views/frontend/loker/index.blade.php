@extends('layouts.app')



@section('content')

    <div class="container">
        <div class="row">
            <div class="col-lg-12 margin-tb">
                <div class="float-left">
                    <h2>Daftar Lowongan Kerja</h2>
                </div>
                <div class="float-right">
                    <a class="btn btn-success" href="{{ route('my-loker.create') }}"> Create New Job</a>
                </div>
            </div>
        </div>

        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif


        <table class="table table-bordered">
            <tr>
                <th>No</th>
                <th>Title</th>
                <th>Available Date</th>
                <th>Description</th>
                <th width="280px">Action</th>
            </tr>
            @foreach ($lokers as $loker)
                <tr>
                    <td>{{ ++$i }}</td>
                    <td>{{ $loker->title }}</td>
                    <td>{{$loker->available_date}}</td>
                    <td>{{ $loker->description }}</td>

                    <td>
                        <form action="{{ route('my-loker.destroy',$loker->id) }}" method="POST">
                            <a class="btn btn-info" href="{{ route('my-loker.show',$loker->id) }}">Show</a>
                            <a class="btn btn-primary" href="{{ route('my-loker.edit',$loker->id) }}">Edit</a>
                            @csrf
                            @method('DELETE')
                            <button type="submit" class="btn btn-danger">Delete</button>
                        </form>
                    </td>
                </tr>
            @endforeach
        </table>
    </div>




    {!! $lokers->links() !!}



@endsection
