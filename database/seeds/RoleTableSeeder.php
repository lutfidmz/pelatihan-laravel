<?php

use Illuminate\Database\Seeder;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = new \App\Http\Models\Role();
        $role->name ='admin';
        $role->save();


        $data['name']='pelamar';

        \App\Http\Models\Role::create($data);

        $data['name']='perusahaan';

        \App\Http\Models\Role::create($data);

    }
}
