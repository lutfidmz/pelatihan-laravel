<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $role = \App\Http\Models\Role::where('name','pelamar')->first();

        $data_user['role_id']=$role->id;
        $data_user['name']='gifary';
        $data_user['email']='muhammadgifary@gmail.com';
        $data_user['password']=bcrypt('secret');
        $data_user['address']='jl bbk tarogong';
        $data_user['phone_number']='083838';
        $data_user['gender']='male';
        $data_user['dob']='1994-03-03';

        $user = \App\User::create($data_user);

        $role = \App\Http\Models\Role::where('name','admin')->first();

        $data_user['role_id']=$role->id;
        $data_user['name']='admin';
        $data_user['email']='admin@gmail.com';
        $data_user['password']=bcrypt('secret');
        $data_user['address']='jl bbk tarogong';
        $data_user['phone_number']='0837383';
        $data_user['gender']='male';
        $data_user['dob']='1994-03-03';

        $user = \App\User::create($data_user);

        $role = \App\Http\Models\Role::where('name','pelamar')->first();

        $data_user['role_id']=$role->id;
        $data_user['name']='perusahaan';
        $data_user['email']='perusahaan@gmail.com';
        $data_user['password']=bcrypt('secret');
        $data_user['address']='jl bbk tarogong';
        $data_user['phone_number']='083838';
        $data_user['gender']='male';
        $data_user['dob']='1994-03-03';

        $user = \App\User::create($data_user);
    }
}
